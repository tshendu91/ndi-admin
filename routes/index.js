'use strict'

// var express = require('express');
const express = require('express');
var url = require('url');
var router = express.Router();
require('dotenv').config();
const debug = require('debug')('my-issuer-app');
const mariadb = require('mariadb');

//evernyms code start
const fs = require('fs')
const axios = require('axios')
// const express = require('express') //repeated
const QR = require('qrcode')
const uuid4 = require('uuid4')
const urljoin = require('url-join');

const ANSII_GREEN = '\u001b[32m'
const ANSII_RESET = '\x1b[0m'
const CRED_DEF_FILE = 'public/images/cred_def_id.txt'
// const PORT = 4000

console.log(process.env.PORT)
//db connection
const pool = mariadb.createPool({
  host: process.env.DB_HOST, 
  user:process.env.DB_USER_NAME, 
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  connectionLimit: 5
});

const verityUrl = process.env.VERITY_URL // address of Verity Application Service

// DCRC domain and api key.
// var domainDid = 'JSEWYK4KKToxmPQQMrmGxn'; 
// var xApiKey = 'Ho2snpJSVBYSYMq2ffDHEaTqpD9H5Eqfg28EsyxXL5Zh:3nXg4v7QvWxKWUXGGmFMNaYm4MY6UgNgeNU5L43XGaVtrJHf6fTFxAn6VfCU1T8uNEFMW7JwSmwzAYw4uvCzfDMp';

var domainDid = process.env.DOMAIN_DID // your Domain DID on the multi-tenant Verity Application Service
var xApiKey = process.env.XAPI_KEY // REST API key associated with your Domain DID

var vasOperationStatus; //to check the operation of VAS response
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/updatewebhook', function(req, res, next) {
  res.render('updatewebhook');
});

router.get('/updatewebhook-verifier', function(req, res, next) {
  res.render('updatewebhookverifier');
});

router.get('/updateconfig', function(req, res, next) {
  res.render('updateconfig');
});

router.get('/updateconfig-verifier', function(req, res, next) {
  res.render('updateconfigverifier');
});

router.get('/setupissuer', function(req, res, next) {
  res.render('setupissuer', {domaindid: domainDid});
});

router.get('/createschema', function(req, res, next) {
  res.render('createschema');
}); 

router.get('/createcreddef', function(req, res, next) {
  res.render('createcreddef', {schemaId: schemaIdGenerated});
});

async function insertIntoTable(query, preparedparam) {
  let conn;
  try {
	  conn = await pool.getConnection();
	  const rows = await conn.query(query, preparedparam);

	  console.log(rows); //[ {val: 1}, meta: ... ]
  } catch (err) {
    console.log(err)
	throw err;
  } finally {
	if (conn) return conn.end();
  }
}

router.get('/webhook_update', async function(req, res) {
  var webhookurl = req.query.webhookurl;
  var webhookurlFinal = webhookurl+'/webhook';  //append /webhook endpoint
  var isIssuer = req.query.isIssuer;
  await updateWebhookEndpoint(webhookurlFinal).then(function(){
    var currentUrl = req.url;
    var urlname = url.parse(currentUrl, true);

    let query = "INSERT INTO t_webhookendpoint (domaindid, webhookendpoint, is_issuer) VALUES (?, ?, ?);";
    insertIntoTable(query, [domainDid, webhookurl, isIssuer]);
    res.render('success', {message: 'Webhook Endpoint Updated Successfully', routename: urlname.pathname});
  }).
  catch(function (err){
    console.log("WRONG ENDPOINT");
    debug("fail")
    res.render('fail', {message: "Something went wrong"});
});
})

router.get('/update_config', async function(req, res) {
  var issuername = req.query.issuername;
  var logourl = req.query.logourl;
  var isIssuer = req.query.isIssuer;
  await updateConfiguration(issuername, logourl);

  var currentUrl = req.url
  var urlname = url.parse(currentUrl, true);

  let query = "INSERT INTO t_configuration (domaindid, issuername, logo, is_issuer) VALUES (?, ?, ?, ?);";
  insertIntoTable(query, [domainDid, issuername, logourl, isIssuer]);

  res.render('success', {message: 'Configuration Updated Successfully', routename: urlname.pathname});
})

var issuerDidResponse = '';
var issuerVerKeyResponse = '';

router.get('/setup_issuer', async function(req, res) {
  await setupIssuer();
  var currentUrl = req.url
  var urlname = url.parse(currentUrl, true);

  let query = "INSERT INTO t_issuer_setup (domaindid, issuerdid, issuerverkey) VALUES (?, ?, ?);";
  insertIntoTable(query, [domainDid, issuerDidResponse, issuerVerKeyResponse]);

  res.render('success', {message: 'Issuer Setup is Successful. <br> <b>Issuer DID:</b> '+issuerDidResponse+'<br><b>VerKey:</b> '+issuerVerKeyResponse+'<br><i>Please send it to Evernym for the endorsement.</i>', routename: urlname.pathname});
})

let schemaIdGenerated
let schemaJsonGenerated

router.get('/create_schema', async function(req, res) {
  var schemaName = req.query.schemaName;
  var attrname = req.query.attrname;
  var attrnameWithSpace = attrname.split(','); //returns element with space in the beginning
  var attrnameArray = attrnameWithSpace.map(el => el.trim()); //to remove space in the beginning of the elemnent
  
  await createSchema(schemaName, attrnameArray);  
  var currentUrl = req.url
  var urlname = url.parse(currentUrl, true);

  let query = "INSERT INTO t_schema (domaindid, schemaname, schema_attribute, schema_id, schemajson) VALUES (?, ?, ?, ?, ?);";
  insertIntoTable(query, [domainDid, schemaName, JSON.stringify(attrnameArray), schemaIdGenerated, schemaJsonGenerated]);
  
  console.log(schemaIdGenerated)
  debug(schemaIdGenerated)
  res.render('success', {message: 'Schema Created Successfully. <br> <b>Schema ID:</b> '+schemaIdGenerated+'<br><b>SchemaJSON</b>: '+schemaJsonGenerated+'<br><i><b>Please send schemaJson to Evernym for the endorsement and save schemaID for future use.</b></i>', routename: urlname.pathname});
})

let credDefIdGenerated
let credDefJsonGenerated

router.get('/create_creddef', async function(req, res) {
  var credname = req.query.credname;
  var revocationStatus = req.query.revokestatus;
  var schemaid = req.query.schemaid;
  
  await createCredDef(credname, revocationStatus, schemaid);  
  var currentUrl = req.url
  var urlname = url.parse(currentUrl, true);

  let query = "INSERT INTO t_credef (domaindid, credef_name, revoke_status, schema_id, credef_id) VALUES (?, ?, ?, ?, ?, ?);";
  insertIntoTable(query, [domainDid, credname, revocationStatus, schemaid, credDefIdGenerated]);

  res.render('success', {message: 'CredDef Created Successfully. <br> <b>CredDef ID:</b> '+credDefIdGenerated+'<br><b>CredDef JSON</b>: '+credDefJsonGenerated+'<br><i><b>Please send CredDef Json to Evernym for the endorsement and save CredDef ID for future use.</b></i>', routename: urlname.pathname});
})

router.get('/success', function(req, res, next) {
  res.render('success', { message: 'Issuer Setup Successful!' });
});

router.get('/fail', function(req, res, next) {
  res.render('fail', { message: 'Issuer Setup Operation Failed!' });
});

//Actual Evernym's method

// Sends a message to the Verity Application Service via the Verity REST API
async function sendVerityRESTMessage (qualifier, msgFamily, msgFamilyVersion, msgName, message, threadId) {
  debug("inside sendverity");
  message['@type'] = `did:sov:${qualifier};spec/${msgFamily}/${msgFamilyVersion}/${msgName}`
  message['@id'] = uuid4()

  if (!threadId) {
    threadId = uuid4()
  }

  // send prepared message to Verity and return Axios request promise
  const url = urljoin(verityUrl, 'api', domainDid, msgFamily, msgFamilyVersion, threadId)
  console.log(`Posting message to ${ANSII_GREEN}${url}${ANSII_RESET}`)
  console.log(`${ANSII_GREEN}${JSON.stringify(message, null, 4)}${ANSII_RESET}`)
  return axios({
    method: 'POST',
    url: url,
    data: message,
    headers: {
      'X-API-key': xApiKey // <-- REST API Key is added in the header
    }
  })
}


// Maps containing promises for the started interactions - threadId is used as the map key
// Update configs
const updateConfigsMap = new Map()
// Setup Issuer
const setupIssuerMap = new Map()
// Schema create
const schemaCreateMap = new Map()
// Credential definition Create
const credDefCreateMap = new Map()

// Map for connection accepted promise - relationship DID is used as the map key
const connectionAccepted = new Map()

// Update webhook protocol is synchronous and does not support threadId
let webhookResolve

async function updateWebhookEndpoint(webhookurl){
  debug("inside updatewebhok function");
  const webhookMessage = {
    comMethod: {
      id: 'webhook',
      type: 2,
      value: webhookurl,
      packaging: {
        pkgType: 'plain'
      }
    }
  }

  const updateWebhook =
  new Promise(function (resolve, reject) {
    webhookResolve = resolve
    sendVerityRESTMessage('123456789abcdefghi1234', 'configs', '0.6', 'UPDATE_COM_METHOD', webhookMessage).catch(function (err){
      console.log("WRONG ENDPOINT");
      reject(new Error('Something went wrong, please enter a valid endpoint'))
    });
  })

  // await updateWebhook
  await updateWebhook.catch(function (err){
    console.log("WRONG ENDPOINT");
    throw new Error("something went wrong...");
});
}

async function updateConfiguration(issuername, logourl){
  const updateConfigMessage = {
    configs: [
      {
        name: 'logoUrl',
        value: logourl
      },
      {
        name: 'name',
        value: issuername
      }
    ]
  }

  const updateConfigsThreadId = uuid4()
  const updateConfigs =
  new Promise(function (resolve, reject) {
    updateConfigsMap.set(updateConfigsThreadId, resolve)
  })

  await sendVerityRESTMessage('123456789abcdefghi1234', 'update-configs', '0.6', 'update', updateConfigMessage, updateConfigsThreadId)

  await updateConfigs
}


async function setupIssuer(){
  // STEP 3 - Setup Issuer
  let issuerDid
  let issuerVerkey

  // check if Issuer Keys were already created
  const getIssuerKeysMsg = {}
  const getIssuerKeysThreadId = uuid4()

  const getIssuerKeys =
  new Promise(function (resolve, reject) {
    setupIssuerMap.set(getIssuerKeysThreadId, resolve)
  })

  await sendVerityRESTMessage('123456789abcdefghi1234', 'issuer-setup', '0.6', 'current-public-identifier', getIssuerKeysMsg, getIssuerKeysThreadId);

  [issuerDid, issuerVerkey] = await getIssuerKeys

  if (issuerDid === undefined) {
    // if issuer Keys were not created, create Issuer keys
    const setupIssuerMsg = {}
    const setupIssuerThreadId = uuid4()
    const setupIssuer =
      new Promise(function (resolve, reject) {
        setupIssuerMap.set(setupIssuerThreadId, resolve)
      })

    await sendVerityRESTMessage('123456789abcdefghi1234', 'issuer-setup', '0.6', 'create', setupIssuerMsg, setupIssuerThreadId);
    [issuerDid, issuerVerkey] = await setupIssuer
    console.log(`Issuer DID: ${ANSII_GREEN}${issuerDid}${ANSII_RESET}`)
    console.log(`Issuer Verkey: ${ANSII_GREEN}${issuerVerkey}${ANSII_RESET}`)
  }
}

async function createSchema(schemaName, attrnameArray){
  // STEP 4 - Create schema and credential definition that will be used for credentials
    const schemaMessage = {
      // name: schemaName + uuid4().substring(0, 8),
      name: schemaName,
      version: '0.1',
      attrNames: attrnameArray
    }
    const schemaThreadId = uuid4()
    const schemaCreate =
      new Promise(function (resolve, reject) {
        schemaCreateMap.set(schemaThreadId, resolve)
      })

    await sendVerityRESTMessage('123456789abcdefghi1234', 'write-schema', '0.6', 'write', schemaMessage, schemaThreadId);
    [schemaIdGenerated, schemaJsonGenerated] = await schemaCreate 
    
    console.log(`Created schema: ${ANSII_GREEN}${schemaIdGenerated}${ANSII_RESET}`)
    console.log(`Created schemaJson: ${ANSII_GREEN}${schemaJsonGenerated}${ANSII_RESET}`)
}

async function createCredDef(credname, revocationStatus, schemaid){
  // if (fs.existsSync(CRED_DEF_FILE)) {
  //   // if the credential definition was created in the previous runs of this app, read credDefId from the file
  //   credDefIdGenerated = fs.readFileSync(CRED_DEF_FILE, 'utf8')
  // } else {
    const credDefMessage = {
      name: credname,
      schemaId: schemaid,
      tag: 'latest',
      revocationDetails: {
        support_revocation: revocationStatus,
        tails_file: "string",
        max_creds: 0
      }
    }
    const credDefThreadId = uuid4()
    const credDefCreate =
      new Promise(function (resolve, reject) {
        credDefCreateMap.set(credDefThreadId, resolve)
      })

    await sendVerityRESTMessage('123456789abcdefghi1234', 'write-cred-def', '0.6', 'write', credDefMessage, credDefThreadId);
    [credDefIdGenerated, credDefJsonGenerated] = await credDefCreate
    console.log(`Created credential definition: ${ANSII_GREEN}${credDefIdGenerated}${ANSII_RESET}`)
    console.log(`Created credential JSON: ${ANSII_GREEN}${credDefJsonGenerated}${ANSII_RESET}`)
    fs.writeFileSync(CRED_DEF_FILE, credDefIdGenerated)
  // } //end else
}

// Verity Application Server will send REST API callbacks to this endpoint
router.post('/webhook', async (req, res) => {
  const message = req.body
  const threadId = message['~thread'] ? message['~thread'].thid : null
  console.log('Got message on the webhook')
  console.log(`${ANSII_GREEN}${JSON.stringify(message, null, 4)}${ANSII_RESET}`)
  res.status(202).send('Accepted')
  // Handle received message differently based on the message type
  switch (message['@type']) {
    case 'did:sov:123456789abcdefghi1234;spec/configs/0.6/COM_METHOD_UPDATED':
      webhookResolve('webhook updated')
      break
    case 'did:sov:123456789abcdefghi1234;spec/update-configs/0.6/status-report':
      updateConfigsMap.get(threadId)('config updated')
      break
    case 'did:sov:123456789abcdefghi1234;spec/issuer-setup/0.6/public-identifier-created':
      issuerDidResponse = message.identifier.did //to assign - SD
      issuerVerKeyResponse = message.identifier.verKey //to assign - SD
      setupIssuerMap.get(threadId)([message.identifier.did, message.identifier.verKey])
      break
    case 'did:sov:123456789abcdefghi1234;spec/issuer-setup/0.6/problem-report':
      if (
        message.message === 'Issuer Identifier has not been created yet'
      ) {
        setupIssuerMap.get(threadId)([undefined, undefined])
      }
      break
    case 'did:sov:123456789abcdefghi1234;spec/issuer-setup/0.6/public-identifier':
      issuerDidResponse = message.did   //to assign - SD
      issuerVerKeyResponse = message.verKey //to assign - SD
      setupIssuerMap.get(threadId)([message.did, message.verKey])
      break
    case 'did:sov:123456789abcdefghi1234;spec/write-schema/0.6/status-report':
      debug("here")
      console.log("here")
      schemaCreateMap.get(threadId)([message.schemaId, message.schemaJson])
      break
    case 'did:sov:123456789abcdefghi1234;spec/write-schema/0.6/needs-endorsement': //SD- Edit
      schemaCreateMap.get(threadId)([message.schemaId, message.schemaJson])
      break
    case 'did:sov:123456789abcdefghi1234;spec/write-cred-def/0.6/status-report':
      credDefCreateMap.get(threadId)([message.credDefId, message.credDefJson])
      break
    case 'did:sov:123456789abcdefghi1234;spec/write-cred-def/0.6/needs-endorsement':
      credDefCreateMap.get(threadId)([message.credDefId, message.credDefJson])
      break
    case 'did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/request-received':
      break
    case 'did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/trust_ping/1.0/sent-response':
      break
    default:
      console.log(`Unexpected message type ${message['@type']}`)
      // if(!message['@type'] == 'did:sov:123456789abcdefghi1234;spec/write-schema/0.6/needs-endorsement'){ //dont terminate if asked for endorsement while writing schema
        process.exit(1)
      // }
  }
})

module.exports = router;
